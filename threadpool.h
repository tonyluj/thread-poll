#ifndef THREADPOOL_H
#define THREADPOOL_H
#include <pthread.h>

typedef struct thread_s thread_t;
typedef struct threadpool_s threadpool_t;
typedef struct job_s job_t;

struct threadpool_s {
    thread_t *threads;
    job_t *jobs;
    int num_jobs;
    int num_threads;
    pthread_mutex_t jobs_mutex;
    pthread_mutex_t num_jobs_mutex;
    pthread_cond_t jobs_not_empty_cond;
    pthread_cond_t jobs_not_full_cond;
};

struct thread_s {
    pthread_t thread_id;
    threadpool_t *pool;
    thread_t *prev;
    thread_t *next;
    int killed;
};

struct job_s {
    void (*job_function)(job_t *job);
    void *user_data;
    job_t *prev;
    job_t *next;
};

extern int threadpool_init(threadpool_t *pool, int numWorkers);

extern void threadpool_shutdown(threadpool_t *pool);

extern void threadpool_add_job(threadpool_t *pool, job_t *job);

extern void threadpool_add_job_ex(threadpool_t *pool, job_t *job);

extern void threadpool_wait(threadpool_t *pool);

#endif // THREADPOOL_H
